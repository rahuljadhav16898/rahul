package Object;

public class MainMobileApp1 {
    public static void main(String[] args){
        Mobile m1=new Mobile();
        System.out.println("COMPANY:"+m1.company);
        System.out.println("PRICE:"+m1.price);
        System.out.println("IS NFC ENABLED:"+m1.isNFCEnable);
        System.out.println("RAM:"+m1.ramSize+"GB");
        m1.calling();
        m1.installApp();
    }
}
