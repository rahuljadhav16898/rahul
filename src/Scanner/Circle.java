package Scanner;

public class Circle{
    //data member
    static double pi=3.14;
    //function members
    void area(double radius)
    {
        double result=pi*radius*radius;
        System.out.println("AREA OF CIRCLE:"+result);

    }
    void circumference(double radius)
    {
        double result=2*pi*radius;
        System.out.println("CIRCUMFERENCE OF CIRCLE:"+result);

    }
}

