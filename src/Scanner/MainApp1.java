package Scanner;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter radius");
        double rad=sc.nextDouble();
        System.out.println("Enter mode of operation");
        System.out.println("1:AREA OF CIRCLE");
        System.out.println("2:CIRCUMFERENCE OF CIRCLE");
        int choice=sc.nextInt();
        if(choice==1)
        {
            //call area method
            new Circle().area(rad);

        }
        else if(choice==2)
        {
            //call circumference method
            new Circle().circumference(rad);

        }
        else
        {
            System.out.println("Invalid choice");
        }

    }
}
