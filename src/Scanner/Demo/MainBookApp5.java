package Scanner.Demo;

public class MainBookApp5 {
    public static void main(String[] args){
        Book b1=new Book();
        b1.acceptdetail("JAVA",25000.25);
        b1.acceptdetail("JAVA",20000.25);
        b1.displaydetails();

        Book b2=new Book();
        b2.acceptdetail("J2EE",45000.20);
        b2.acceptdetail("J2EE",35000);
        b2.displaydetails();
    }
}
