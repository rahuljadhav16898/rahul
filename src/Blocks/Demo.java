package Blocks;

public class Demo{
    static{
        System.out.println("static Block1 of Demo");

    }
    static{
        System.out.println("static Block2 of Demo");
    }
}

class BlockDemo{
    static {
        System.out.println("static Block of main class");
    }
    public static void main(String[] args){
        System.out.println("MAIN METHOD");
        Demo d1=new Demo();

    }
}
