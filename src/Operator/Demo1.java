package Operator;

public class Demo1 {
    public static void main(String[] args){
        int a=36;
        int b=25;
        int c=35;
        System.out.println(a>b&&b<c);
        System.out.println(c>a || b>a);
        System.out.println((c<a && b<a)||(b>a && a<c));
        System.out.println(!(c>a));
        System.out.println(!(a>b && b>c));
    }
}
